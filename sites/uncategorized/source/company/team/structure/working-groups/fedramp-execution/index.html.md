---
layout: markdown_page
title: "FedRAMP Execution Working Group"
description: "The charter of this working group is to drive execution of FedRAMP compliance."
canonical_path: "/company/team/structure/working-groups/fedramp-execution/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value             |
|-----------------|-------------------|
| Date Created    | November 25, 2020 |
| End Date        | TBD               |
| Slack           | [#wg_fedramp-execution](https://gitlab.slack.com/archives/C02LT4Q96CC) (only accessible from within the company) |
| Google Doc      | Search "FedRAMP Execution Working Group Agenda" in Google Drive (only accessible from within the company) |
| Issue Board     | TBD             |

### Exit Criteria

This working group will organize all the domain experts needed, surface critical decisions, centralize status, and drive execution. More specific exit criteria will be developed early in the process.

## Roles and Responsibilities

| Working Group Role             | Team Member     | Functional Title                           |
|--------------------------------|-----------------|--------------------------------------------|
| Facilitator                    | Johnathan Hunt  | VP of Security                             |
| Functional Lead: PM            | Josh Lambert    | Director of Product Management, Enablement |
| Functional Lead: TBD           | TBD             | TBD                                        |
| Executive Stakeholder          | Eric Johnson    | Chief Technology Officer                   |
